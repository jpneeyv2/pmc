$(window).on('load', function(){
    $(".se-pre-con").fadeOut();
    $(".se-pre-con").css({
        "display" : "none"
    });
    header();

})

$(window).scroll(function() {   
    header();
});

$(document).ready(function () {
    $('.banner').fadeIn(1300).removeClass('hidden');
    /* ease(); */
    /* bannerCarousel(); */
    /* $('.logo').on('click', function(){
        bannerCarousel();
    }) */
});

/* functions */
function bannerCarousel(){
    let img = ['assets/main.jpg', 'assets/who.jpg', 'assets/vision.jpg'];
    let e = 0;
    let b = img.length - 1;
    setInterval(function(){
        $('div.banner.home').stop().animate({opacity: 0},1000,function(){

            $(this).css({
                'background-image' : "url(" +img[e]+ ")"
            }).animate({opacity: 1},{duration:1000});
            })
            if (e == b){
            e = 0;
            } else {
            e++;  
            }
        }, 5000
    );
}
function falink(e){
    switch (e){
        case 1:
           window.location.href = "./pages/about-us.html";
           break;
        case 2:
            window.location.href = "http://pmc.ph/products";
            break;
        default:
           window.location.href = "./pages/about-us.html";
           break;
    }
}
function header(){
    var ep = $('.rows').offset().top;
    var sh = $(window).height();
    var ao = 0.5;
    var ap = ep - (sh * ao);
    var msh = $('body').height() - sh - 5;

    $(window).on('scroll', function() {
        var ys = window.pageYOffset;

        var ev = ys > ap;
        var h = msh <= ys && !ev;

        if(ev || h) {
            $(".logo").css({
                "width" : "80%",
                "transition" : "0.5s"
            });
            $('.to-top').css({
                "display" : "block"
            });
            
            $('.web-a').css({
                "font-size" : "10px",
                "transition" : "0.2s"
            })
            $('.mobile-menu-bars').css({
                "padding" : "7px",
                "transition" : "0.5s"
            })
            /* $('.web-menu').css({
                "padding" : "7px",
                "transition" : "0.5s"
            }) */
        }
        else {
            $(".logo").css({
                "width" : "100%",
                "transition" : "0.5s"
            });
            
            $('.to-top').css({
                "display" : "none",
            })
            
            $('.web-a').css({
                "font-size" : "12px",
                "transition" : "0.2s"
            })
            $('.mobile-menu-bars').css({
                "padding" : "15px",
                "transition" : "0.2s"
            })
        }
    });
}


function scrollMe(e){
    if(e === 1){
        $('html, body').animate({ scrollTop:  $('.rows').offset().top - 50 }, 'slow');}
        else{$('html, body').animate({ scrollTop:  $('.banner').offset().top - 50 }, 'slow');}
}


function mobileMenu(e) {
    switch(e) {
        case 1:
            $('.mobile-menu').css({
                "top" : "0",
                "transition" : "0.5s"
            })
            break;
        case 2:
            $('.mobile-menu').css({
                "top" : "-100vh",
                "transition" : "0.3s"
            })
             break;
        default:
            alert("---");
       }  
}


function ease(){

    var win = $(window);
    var allMods = $("#trans > div > div > div");
    
    // Already visible modules
    allMods.each(function(i, el) {
      var el = $(el);
      if (el.visible(true)) {
        el.addClass("already-visible"); 
      } 
    });
    
    win.scroll(function(event) {
      
      allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
          el.addClass("come-in"); 
        } 
      });
      
    });

}

function submit() {
    var x;
    x = $("input").value;
    if (x == "") {
        alert("Please enter the fields with correct value.");
        return false;
    };
}

$(document).ready(function () {
    var $form = $('#mc-embedded-subscribe-form')
    if ($form.length > 0) {
        $('#mc-embedded-subscribe-form input[type="submit"]').bind('click', function (event) {
        if (event) event.preventDefault()
        register($form)
        })
    }
    })

    function register($form) {
    $('#mc-embedded-subscribe').val('Sending...');
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
        success: function (data) {
        $('#mc-embedded-subscribe').val('subscribe')
        if (data.result === 'success') {
            $('#mce-EMAIL').css('borderColor', '#ffffff')
            $('#subscribe-result').css('color', 'rgb(53, 114, 210)')
            $('#subscribe-result').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>')
            $('#mce-EMAIL').val('')
        } else {
            $('#mce-EMAIL').css('borderColor', '#ff8282')
            $('#subscribe-result').css('color', '#ff8282')
            $('#subscribe-result').html('<p>' + data.msg.substring(4) + '</p>')
        }
        }
    })
};